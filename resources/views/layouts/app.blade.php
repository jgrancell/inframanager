@extends('layouts.base')

@section('content')
<div class="container-fluid ml-0 mr-0 w-100 infrastructure" style="height: 88%; margin-top: 44px; position: relative;">
    <div class="row pl-3 pt-2 pb-1" style="background-color: #fff;">
        @yield('titlebar')
    </div>

    <div class="row" style="height: 100%;">
        <div class="col-md-12 pl-0 pr-0">
            @yield('main')
        </div>
    </div>
</div>
@endsection
