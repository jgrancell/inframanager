@extends('layouts.app')

@section('titlebar')
    <script>var netdataNoBootstrap = true;</script>
    <script type="text/javascript" src="http://{{ $device->ip_address }}:19999/dashboard.js"></script>

    <div class="col-md-8">
        <h1><a href="/infrastructure" style="color: inherit; text-decoration: none;">Infrastructure</a> > <a style="color: inherit; text-decoration: none;" href="/infrastructure/{{$infrastructure->id }}">{{ $infrastructure->name }}</a> > {{ $device->hostname }}</h1>
    </div>
    <div class="col-md-4">
        @if ($device->hasMonitoring())
            <a class="float-right btn btn-secondary my-1" href="/infrastructure/{{ $infrastructure->id }}/device/{{ $device->id }}/monitoring/disable">Disable Monitoring</a>
        @else
            <a class="float-right btn btn-success my-1" href="/infrastructure/{{ $infrastructure->id }}/device/{{ $device->id }}/monitoring/enable">Enable Monitoring</a>
        @endif
    </div>
@endsection

@section('main')
    <div class="d-flex flex-row justify-content-between bg-dark px-5" style="color: #e5e5e5; font-weight: bold; text-align: center;">
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">IP Address</p>
            <p class="m-1 py-2">{{ $device->ip_address }}</p>
        </div>
        <div class="flex-fill py-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Monitoring</p>
            <p class="m-1 py-2">
                <a href="http://{{ $device->ip_address }}:19999" target="blank">
                    @if ($device->hasMonitoring())
                        @if ($device->hasAlarm())
                            <i style="color: red;" class="fas fa-lg fa-exclamation-triangle"></i>
                        @else
                            <i style="color: green;" class="fas fa-lg fa-exclamation-triangle"></i>
                        @endif
                    @else
                        <i style="color: gray;" class="fas fa-lg fa-times-circle"></i>
                    @endif
                </a>
            </p>
        </div>
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Type</p>
            <p class="m-1 py-2">{{ ucwords($device->type) }}</p>
        </div>
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Hypervisor</p>
            <p class="m-1 py-2">{{ ucwords($device->hypervisor) }}</p>
        </div>
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Role</p>
            <p class="m-1 py-2">{{ $device->role }}</p>
        </div>
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Puppetized</p>
            @if ($device->isPuppetized())
                <p class="m-1 py-2"><i class="fas fa-lg fa-check-circle" style="color: lightgreen;"></i></p>

            @else
                <p class="m-1 py-2"><i class="fas fa-lg fa-times-circle" style="color: gray;"></i></p>
            @endif
        </div>
    </div>

    <div class="row" style="height: 100%;">
        <div class="col-md-3 px-5 py-2 infr-left-col">
            <div class="row">
                <h3 style="text-align: center;">Alarms</h3>
                @foreach ($device->getAlarms() as $alarm)
                    <div data-netdata="{{ $alarm->chart }}"></div>
                @endforeach
            </div>
        </div>
        <div class="col-md-9 pl-0 pr-0">
            <div class="d-flex flex-row px-4 py-2">
            </div>
        </div>
    </div>
@endsection
