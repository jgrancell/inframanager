@extends('layouts.infrastructure')

@section('titlebar')
<div class="col-md-6">
    <h1><a href="/infrastructure" style="text-decoration: none;">Infrastructure</a> > {{ $infrastructure->name }} > Add Device</h1>
</div>
@endsection

@section('left-column')
<div class="mx-2 mt-3">
    <h3>What Is A Device?</h3>
    <p>
        An device is a single server or machine. This can be a firewall,
        rack-mounted server, desktop computer, virtual server, hypervisor,
        network switch, wireless router, or anything really.
    </p>
    <h3>What Infrastructure Should I Attach To?</h3>
    <p>
        Devices can only be attached to a single infrastructure, so you want to
        attach it based on a few different criteria (in order of importance):
    </p>
    <ul>
        <li>What configuration management environment is it a part of?</li>
        <li>What Network CIDR is the IP Address part of?</li>
        <li>What is the primary purpose or role of the server?</li>
    </ul>
    <p>
        If this device shares any of the above with another device, consider adding
        them both to the same infrastructure. Otherwise, create a new infrastructure
        for the device.
    </p>
    <h3>How To Add A Device</h3>
    <p>Adding a device to an infrastructure requires 8 pieces of information:</p>
    <ul>
        <li>Device hostname (must be unique)</li>
        <li>IP Address</li>
        <li>NetData Monitoring URL</li>
        <li>Device Type</li>
        <li>Hypervisor (if Virtualized)</li>
        <li>Device Role</li>
        <li>Is the device Puppetized?</li>
        <li>Any important notes</li>
    </ul>
</div>
@endsection

@section('right-column')
<div class="col-md-12">
    <form action="/infrastructure/{{ $infrastructure->id }}/device/create" method="POST">
        {{ csrf_field() }}
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="infrastructure_id">Infrastructure</label>
                <input type="text" class="form-control" id="infrastructure_id" name="infrastructure_id" value="{{ $infrastructure->id }}" placeholder="{{ $infrastructure->name }}" readonly>
            </div>
            <div class="form-group col-md-6">
                <label for="hostname">Hostname</label>
                <input type="text" class="form-control" id="hostname" name="hostname" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="ip_address">IP Address</label>
                <input type="text" class="form-control" id="ip_address" name="ip_address" required>
            </div>
            <div class="form-group col-md-6">
                <label for="netdata">Enable NetData Monitoring:</label>
                <select class="form-control" id="netdata" name="netdata">
                    <option value="0" selected>No</option>
                    <option value="1">Yes</option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="type">Device Type</label>
                <select class="form-control" id="type" name="type" required>
                    <option value="virtualized">Virtualized</option>
                    <option value="physical">Physical</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="hypervisor">Host Hypervisor:</label>
                <input type="text" class="form-control" id="hypervisor" name="hypervisor" value="N/A" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="role">Device Role</label>
                <input type="text" class="form-control" id="role" name="role" required>
            </div>
            <div class="form-group col-md-6">
                <label for="puppetized">Is This Puppetized:</label>
                <select class="form-control" id="puppetized" name="puppetized" required>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
            </div>
        </div>
        <input type="reset" class="btn btn-danger btn-lg" name="reset" value="Clear">
        <input type="submit" class="btn btn-success btn-lg" name="submit" value="Create Infrastructure">
    </form>
</div>

@endsection
