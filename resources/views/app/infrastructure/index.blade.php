@extends('layouts.infrastructure')

@section('titlebar')
<div class="col-md-6">
    <h1>Infrastructure Overview</h1>
</div>
<div class="col-md-6">
    <a class="float-right btn btn-primary my-1" href="/infrastructure/create">New Infrastructure</a>
</div>
@endsection

@section('left-column')

@endsection

@section('right-column')
<table class="table">
    <thead class="infr-table-head">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Network CIDR</th>
            <th scope="col">Type</th>
            <th scope="col">Vendor</th>
            <th scope="col">Location</th>
            <th scope="col">Attached Devices</th>
            <th scope="col">Puppet Environment</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($infrastructures as $infra)
            <tr>
                <th scope="row"><a class="infra-link" href="/infrastructure/{{ $infra->id }}">{{ $infra->name }}</a></th>
                <td>{{ $infra->cidr }}</td>
                <td>{{ $infra->type }}</td>
                <td>{{ $infra->vendor}}</td>
                <td>{{ $infra->location }}</td>
                <td>{{ $infra->devices()->count()}}</td>
                <td>{{ $infra->puppet_environment }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection
