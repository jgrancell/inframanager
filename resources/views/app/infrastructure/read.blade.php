@extends('layouts.app')

@section('titlebar')
    <div class="col-md-6">
        <h1><a href="/infrastructure" style="text-decoration: none;">Infrastructure</a> > {{ $infrastructure->name }}</h1>
    </div>
    <div class="col-md-6">
        <a class="float-right btn btn-primary my-1" href="/infrastructure/{{ $infrastructure->id }}/device/create">Add Device</a>
        <a class="float-right btn btn-primary my-1 mr-1" href="/infrastructure/{{ $infrastructure->id }}/application/create">Add Application</a>
    </div>
@endsection

@section('main')
    <div class="d-flex flex-row justify-content-between bg-dark px-5" style="color: #e5e5e5; font-weight: bold; text-align: center;">
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Network CIDR</p>
            <p class="m-1 py-2">{{ $infrastructure->cidr }}</p>
        </div>
        <div class="flex-fill py-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Device Types</p>
            <p class="m-1 py-2">{{ ucwords($infrastructure->type) }}</p>
        </div>
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Vendor</p>
            <p class="m-1 py-2">{{ ucwords($infrastructure->vendor) }}</p>
        </div>
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Geographic Location</p>
            <p class="m-1 py-2">{{ ucwords($infrastructure->location) }}</p>
        </div>
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Puppet Environment</p>
            <p class="m-1 py-2"><span class="badge badge-primary p-2" style="font-size: 14px;">{{ $infrastructure->puppet_environment }}</span></p>
        </div>
        <div class="flex-fill p-2 border-right border-secondary">
            <p class="m-0 py-2 border-bottom border-faded">Attached Devices</p>
            <p class="m-1 py-2">{{ $infrastructure->devices()->count() }}</p>
        </div>
        <div class="flex-fill p-2">
            <p class="m-0 py-2 border-bottom border-faded">Supported Applications</p>
            <p class="m-1 py-2">Not Yet Available</p>
        </div>
    </div>

    <div class="row" style="height: 100%;">
        <div class="col-md-3 px-5 py-2 infr-left-col">
            <div class="row">
                @foreach ($infrastructure->devices()->orderBy('hostname', 'asc')->get() as $device)
                    @if ($device->hasMonitoring())
                        <div class="card mr-3 mb-1" style="width: 12rem;">
                            <div class="card-header" style="text-align: center; @if ($device->hasAlarm()){{"background-color: darkred; color: white;"}}@endif">
                                {{ str_replace('.campbellmarketing.net', '', $device->hostname) }}
                            </div>
                            <div class="card-body d-flex flex-row justify-content-between px-2">
                                <div class="flex-fill border-right border-primary" style="text-align: center">
                                    <p>{{ $device->alarmCount('normal')}}</p>
                                    <p>TOT</p>
                                </div>
                                <div class="flex-fill border-right border-secondary" style="text-align: center;">
                                    <p>{{ $device->alarmCount('warning') }}</p>
                                    <p>WARN</p>
                                </div>
                                <div class="flex-fill" style="text-align: center;">
                                    <p>{{ $device->alarmCount('critical') }}</p>
                                    <p>CRIT</p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="col-md-9 pl-0 pr-0">
            <div class="d-flex flex-row">
                <table class="table">
                    <thead class="infr-table-head">
                        <tr>
                            <th scope="col">Hostname</th>
                            <th scope="col" style="text-align: center;">IP Address</th>
                            <th scope="col" style="text-align: center;">Monitoring</th>
                            <th scope="col" style="text-align: center;">Type</th>
                            <th scope="col" style="text-align: center;">Hypervisor</th>
                            <th scope="col" style="text-align: center;">Role</th>
                            <th scope="col" style="text-align: center;">Puppetized</th>
                            <th scope="col">Note</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($infrastructure->devices()->orderBy('ip_address', 'asc')->get() as $device)
                            <tr>
                                <th scope="row"><a class="infra-link" href="/infrastructure/{{ $infrastructure->id }}/device/{{ $device->id }}">{{ $device->hostname }}</a></th>
                                <td style="text-align: center;">{{ $device->ip_address }}</td>
                                <td style="text-align: center">
                                    <a href="http://{{ $device->ip_address }}:19999" target="blank">
                                        @if ($device->hasMonitoring())
                                            @if ($device->hasAlarm())
                                                <i style="color: red;" class="fas fa-lg fa-exclamation-triangle"></i>
                                            @else
                                                <i style="color: green;" class="fas fa-lg fa-exclamation-triangle"></i>
                                            @endif
                                        @else
                                            <i style="color: gray;" class="fas fa-lg fa-times-circle"></i>
                                        @endif
                                    </a>
                                </td>
                                <td style="text-align: center;">{{ $device->type }}</td>
                                <td style="text-align: center;">{{ $device->hypervisor }}</td>
                                <td style="text-align: center;">{{ $device->role }}</td>
                                @if ($device->isPuppetized())
                                    <td style="text-align: center;"><i class="fas fa-lg fa-check-circle" style="color: green;"></i></td>
                                @else
                                    <td style="text-align: center;"><i class="fas fa-lg fa-times-circle"></i></td>
                                @endif

                                <td>Primary staging webserver</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
