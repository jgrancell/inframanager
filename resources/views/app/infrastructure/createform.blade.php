@extends('layouts.infrastructure')

@section('titlebar')
<div class="col-md-6">
    <h1><a href="/infrastructure" style="text-decoration: none;">Infrastructure</a> > Create New Infrastructure</h1>
</div>
@endsection

@section('left-column')
<div class="mx-2 mt-3">
    <h3>What Is An Infrastructure?</h3>
    <p>
        An infrastructure is a group of devices that serve a common purpose. This can
        be anything from a production LAMP stack, a Kubernetes/Docker Swarm cluster,
        a full SMB Windows-based deployment, or any variety of devices working together
        for a common purpose.
    </p>
    <h3>Why Split Devices Up?</h3>
    <p>
        Typically, devices are split up by infrastructure because there are specific
        characteristics that differentiate them from other groups of devices.
    </p>
    <p>
        While you could, for example, create a single Infrastructure that includes
        ALL production servers running Apache, PHP, and MySQL it may make more sense
        to separate them into different infrastructures if their configurations are
        radically different or if they're on different vendors' cloud environments.
    </p>
    <h3>How To Create An Infrastructure</h3>
    <p>Creating an infrastructure requires at a minimum 6 pieces of information:</p>
    <ul>
        <li>Infrastructure name (must be unique)</li>
        <li>The network IP address CIDR range assigned to the devices within the infrastructure.</li>
        <li>The type of devices in the infrastructure: Physical, Virtualized, or Mixed.</li>
        <li>The vendor that provides hosting for the devices. Self-hosted is an acceptable answer.</li>
        <li>The location of the devices. This could be your office or datacenter, or a cloud availability zone/datacenter.</li>
        <li>The Puppet Environment that these devices are assigned to pull catalogs from.</li>
    </ul>
</div>
@endsection

@section('right-column')
<div class="col-md-12">
    <form action="/infrastructure/create" method="POST">
        {{ csrf_field() }}
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group col-md-6">
                <label for="cidr">Assigned Network CIDR</label>
                <input type="text" class="form-control" id="cidr" name="cidr" placeholder="0.0.0.0/0" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="type">Device Types:</label>
                <select class="form-control" id="type" name="type" required>
                    <option selected disabled>Choose a Device Type...</option>
                    <option value="mixed">Mixed</option>
                    <option value="physical">Physical</option>
                    <option value="virtualized">Virtualized</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="vendor">Vendor</label>
                <input type="text" class="form-control" id="vendor" name="vendor" placeholder="Amazon Web Services, Self-hosted, etc" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="location">Geographic Location:</label>
                <input type="text" class="form-control" id="location" name="location" required>
            </div>
            <div class="form-group col-md-6">
                <label for="puppet_environment">Puppet Environment / Configuration Management Identifier</label>
                <input type="text" class="form-control" id="puppet_environment" name="puppet_environment" required>
            </div>
        </div>
        <input type="reset" class="btn btn-danger btn-lg" name="reset" value="Clear">
        <input type="submit" class="btn btn-success btn-lg" name="submit" value="Create Infrastructure">
    </form>
</div>

@endsection
