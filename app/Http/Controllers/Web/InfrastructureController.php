<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Infrastructure;

class InfrastructureController extends Controller
{

    private $active = 'infrastructure';

    /**
     * Route: /infrastructure
     * Name: infrastructure
     * Method: GET
     */
    public function index()
    {
        $infrastructures = Infrastructure::orderBy('name')->get();
        $active = $this->active;
        return view('app.infrastructure.index', compact('active', 'infrastructures'));
    }

    /**
     * Route: /infrastructure/create
     * Method: GET
     */
    public function createform()
    {
        $active = $this->active;
        return view('app.infrastructure.createform', compact('active'));
    }

    /**
     * Route: /infrastructure/create
     * Method: POST
     */
    public function create(Request $request)
    {
        $infra = Infrastructure::firstOrNew(array('name' => $request->input('name')));
        $infra->name = $request->input('name');
        $infra->cidr = $request->input('cidr');
        $infra->type = $request->input('type');
        $infra->vendor = $request->input('vendor');
        $infra->location = $request->input('location');
        $infra->puppet_environment = $request->input('puppet_environment');
        $infra->save();

        return redirect('/infrastructure');
    }

    /**
     * Route: /infrastructure/{id}
     * Method: GET
     */
    public function read(Infrastructure $infrastructure)
    {
        $active = $this->active;
        return view('app.infrastructure.read', compact('active', 'infrastructure'));
    }
}
