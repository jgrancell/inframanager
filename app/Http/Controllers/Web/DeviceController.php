<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Device;
use App\Infrastructure;

class DeviceController extends Controller
{
    private $active = 'infrastructure';

    /**
     * Route: /infrastructure/{infastructure}/device/create
     * Method: GET
     */
    public function createform(Infrastructure $infrastructure)
    {
        $active = $this->active;
        return view('app.infrastructure.device.createform', compact('active', 'infrastructure'));
    }

    /**
     * Route: /infrastructure/{infrastructure}/device/create
     * Method: POST
     */
    public function create(Request $request, Infrastructure $infrastructure)
    {
        $device = Device::firstOrNew(array('hostname' => $request->input('hostname')));
        $device->infrastructure_id = $request->input('infrastructure_id');
        $device->ip_address = $request->input('ip_address');
        $device->netdata    = $request->input('netdata');
        $device->type       = $request->input('type');
        $device->hypervisor = $request->input('hypervisor');
        $device->role       = $request->input('role');
        $device->puppetized = $request->input('puppetized');
        $device->save();

        return redirect('/infrastructure/' . $infrastructure->id);
    }

    /**
     * Route: /infrastructure/{infrastructure}/device/{device}
     * Method: GET
     */
    public function read(Infrastructure $infrastructure, Device $device)
    {
        $active = $this->active;
        return view('app.infrastructure.device.read', compact('active', 'infrastructure', 'device'));
    }

    /**
     * Route: /infrastructure/{infrastructure}/device/{device}/monitoring/{type}
     * Method: Get
     */
    public function monitoring(Infrastructure $infrastructure, Device $device, $type)
    {
        $netdata = 0;
        if ($type == 'enable') {
            $netdata = 1;
        }
        $device->netdata = $netdata;
        $device->save();

        return redirect('infrastructure/' . $infrastructure->id . '/device/' . $device->id);
    }
}
