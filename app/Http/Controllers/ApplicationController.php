<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * Route: /
     * Name: dashboard
     */
    public function index()
    {
        $active = 'dashboard';
        return view('app.index', compact('active'));
    }
}
