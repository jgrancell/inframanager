<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infrastructure extends Model
{
    //
    protected $fillable = [
        'name',
    ];

    public function devices()
    {
        return $this->hasMany('App\Device');
    }

    public function applications()
    {
        return [1];
    }
}
