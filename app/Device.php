<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Device extends Model
{

    protected $fillable = [
        'hostname'
    ];

    public function infrastructure()
    {
        return $this->belongsTo('App\Infrastructure');
    }

    public function isPuppetized()
    {
        if ($this->puppetized == 1) {
            return true;
        }
        return false;
    }

    public function hasMonitoring()
    {
        if ($this->netdata == 1) {
            return true;
        }
        return false;
    }

    public function hasAlarm()
    {
        if ($this->hasMonitoring()) {
            $client = new Client();
            try {
                $response = $client->request(
                    'GET',
                    'http://' . $this->ip_address . ':19999/api/v1/info'
                );

                $data = json_decode($response->getBody());

                if (isset($data->alarms)) {
                    if ($data->alarms->warning >= 1 || $data->alarms->critical >= 1) {
                        return true;
                    }
                }
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function getAlarms()
    {
        if ($this->hasMonitoring()) {
            $client = new Client();
            try {
                $response = $client->request(
                    'GET',
                    'http://' . $this->ip_address . ':19999/api/v1/alarms'
                );

                $data = json_decode($response->getBody());

                return $data->alarms;
            } catch (\Exception $e) {
                return [];
            }
        }
    }

    public function alarmCount($type)
    {
        $alarmCount = [
            'normal' => 0,
            'warning' => 0,
            'critical' => 0
        ];
        try {
            if ($this->hasMonitoring()) {
                $client = new Client();
                $response = $client->request(
                    'GET',
                    'http://' . $this->ip_address . ':19999/api/v1/info'
                );

                $data = json_decode($response->getBody());

                if (isset($data->alarms)) {
                    $alarmCount = [
                        'normal' => $data->alarms->normal,
                        'warning' => $data->alarms->warning,
                        'critical' => $data->alarms->critical,
                    ];
                }
            }
        } catch (\Exception $e) {
            return '---';
        }
        return $alarmCount[$type];
    }
}
