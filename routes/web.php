<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ApplicationController@index')->name('dashboard');

Auth::routes();

Route::group(['namespace' => 'Web'], function () {
    Route::group(['prefix' => 'infrastructure'], function () {
        Route::middleware(['auth'])->group(function () {
            Route::get('/', 'InfrastructureController@index');

            // Manipulating Infrastructures
            Route::get('/create', 'InfrastructureController@createform');
            Route::post('/create', 'InfrastructureController@create');
            Route::get('/{infrastructure}', 'InfrastructureController@read');
            Route::post('/{infrastructure}', 'InfrastructureController@update');
            Route::delete('/{infrastructure}', 'InfrastructureController@delete');

            // Manipulating Devices
            Route::get('/{infrastructure}/device/create', 'DeviceController@createform');
            Route::post('/{infrastructure}/device/create', 'DeviceController@create');
            Route::get('/{infrastructure}/device/{device}', 'DeviceController@read');
            Route::get('/{infrastructure}/device/{device}/monitoring/{type}', 'DeviceController@monitoring');
        });
    });
});
